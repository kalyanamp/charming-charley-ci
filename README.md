# [Phing](https://www.phing.info/) Build script for ONESTOP_CHCH

## About
This is slightly customized version of build from here: https://stash.gorillagroup.com/projects/MAG2/repos/m2ee-ci/browse.

Please refer to this doc: https://confluence.gorillagroup.com/display/PHP/Magento+2+Continuous+Integration to understand the main flow.
It can be slightly changed however.

## Target description

- **magento2-build** - Magento 2 main build
